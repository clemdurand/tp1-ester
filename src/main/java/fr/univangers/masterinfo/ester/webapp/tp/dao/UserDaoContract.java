package fr.univangers.masterinfo.ester.webapp.tp.dao;

import java.util.List;

import fr.univangers.masterinfo.ester.webapp.tp.bean.UserBean;

/**
 * UserDaoContract
 */
public interface UserDaoContract {
	
	/**
	 * @param id
	 * @return un utilisateur selon son id
	 */
	UserBean findOne(final String id);
	
	/**
	 * @return la liste de tous les utilisateurs trouvés
	 */
	List<UserBean> findAll();
	
	/**
	 * Enregistrer un utilisateur
	 *
	 * @param user
	 */
	void save(final UserBean user);
	
	/**
	 * Mettre à jour un utilisateur
	 *
	 * @param user
	 */
	void update(final UserBean user);
	
	/**
	 * Supprimer un utilisateur
	 *
	 * @param user
	 */
	void deleteOne(final UserBean user);
	
	/**
	 * Supprimer tous les utilisateurs
	 */
	void deleteAll();
	
	/**
	 * Vérifier si un utilisateur existe par son login
	 * 
	 * @param login
	 * @return boolean
	 */
	boolean existUser(String login);
	
	/**
	 * Permet de retrouver un utilisateur par son login
	 * 
	 * @param login
	 * @return UserBean
	 */
	UserBean findUser(String login);

}
