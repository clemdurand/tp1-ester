package fr.univangers.masterinfo.ester.webapp.tp.bean;

import java.util.Arrays;
import java.util.List;

/**
 * UserFakeData
 */
public class UserFakeData {

	/**
	 * Constructeur privé
	 */
	private UserFakeData() {
		
	}
	
	/**
	 * @return une liste d'utilisateur à enregistrer dans la BD
	 */
	public static List<UserBean> getListUsers() {
		
		UserBean userAdmin = new UserBean();
		userAdmin.setLogin("admin");
		userAdmin.setPassword("admin");
		
		UserBean userDev = new UserBean();
		userDev.setLogin("developpeur");
		userDev.setPassword("developpeur");
		
		UserBean userDirecteur = new UserBean();
		userDirecteur.setLogin("directeur");
		userDirecteur.setPassword("directeur");
		
		return Arrays.asList(userAdmin, userDev, userDirecteur);
	}
}
