package fr.univangers.masterinfo.ester.webapp.tp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * UserBean
 */
@Entity
@Table(name = "t_user")

public class UserBean {

	/**
	 * Identifiant unique de l'utilisateur
	 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	/**
	 * Le login de l'utilisateur
	 */
	@Column(name = "use_login")
	private String login;

	/**
	 * Le mot de passe de l'utilisateur
	 */
	@Column(name = "use_password")
	private String password;

	
	public String getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
