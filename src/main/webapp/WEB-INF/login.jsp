<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
</head>
<body>
	<label style="color: red">${error}</label>

	<form method="POST" action="LoginServlet">
		Nom : <input type="text" name="name" /><br> 
		Mot de passe : <input type="password" name="password" /><br /> 
		<input type="submit" value="Se connecter">
	</form>
</body>
</html>